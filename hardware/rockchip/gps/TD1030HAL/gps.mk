###############################################################################
# GPS HAL libraries
LOCAL_PATH := $(call my-dir)

PRODUCT_COPY_FILES += \
			hardware/rockchip/gps/TD1030HAL/AGNSS/libtdcrypto.so:system/lib64/libtdcrypto.so \
			hardware/rockchip/gps/TD1030HAL/AGNSS/libtdssl.so:system/lib64/libtdssl.so \
			hardware/rockchip/gps/TD1030HAL/AGNSS/libtdsupl.so:system/lib64/libtdsupl.so \
			hardware/rockchip/gps/TD1030HAL/AGNSS/supl-client:system/bin/supl-client \
			hardware/rockchip/gps/TD1030HAL/AGNSS/tdgnss.conf:system/etc/tdgnss.conf


